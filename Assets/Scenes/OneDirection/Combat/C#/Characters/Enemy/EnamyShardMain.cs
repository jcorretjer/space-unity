﻿using Assets.Scenes.OneDirection.Combat.C_.Models;
using UnityEngine;

public class EnamyShardMain : MonoBehaviour
{
    private MeshRenderer render;

    /// <summary>
    /// Flag that indicates if the fadeout animation should start
    /// </summary>
    private bool startFadeout = false;

    private GameObject shardObject;

    /// <summary>
    /// Sets the anmation loop to true and alows it to run
    /// </summary>
    /// <param name="gameObj">The game object that's going to fade out</param>
    public void StartFadeout(GameObject gameObj)
    {
        startFadeout = true;

        shardObject = gameObj;

        render = gameObj.GetComponent<MeshRenderer>();
    }

    /// <summary>
    /// How much it's going to be substracted from the current alpha
    /// </summary>
    //private const float ALPHA_SUBSTRACTOR = 0.005f;

    private const float ALPHA_SUBSTRACTOR = 0.025f;

    private static GameObject parentGameObj;

    void Update()
    {
        if (startFadeout)
        {          
            //Delete it because it's not visible and not necesary
            if (CamContainerMain.GetHitCount() == CamContainerMain.GetSpwn().TotalAmount || render.material.color.a <= 0)
            {
                startFadeout = false;

                /*
                 * For some reason this else block keeps looping even after destroyed.
                 * To avoid miscounting the enemies alive I verify if it's looping in the same parent gameobj. 
                 * If it is, it measn it's being invoked by the same group of shards and it's still not destroyed
                 */
                if (parentGameObj != shardObject.transform.parent.gameObject.transform.parent.gameObject)
                    Spawner.enemiesAliveCount--;

                Destroy(shardObject.transform.parent.gameObject.transform.parent.gameObject);

                parentGameObj = shardObject.transform.parent.gameObject.transform.parent.gameObject;
            }

            //> 0 means que todavia esta visible
            else if (render.material.color.a > 0)
            {
                foreach (var material in render.materials)
                    material.color = new Color(material.color.r, material.color.g, material.color.b, material.color.a - ALPHA_SUBSTRACTOR);
            }
        }
    }
}
