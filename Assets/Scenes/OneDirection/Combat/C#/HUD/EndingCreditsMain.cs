﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using static Assets.Scenes.C_.Models.Game;

public class EndingCreditsMain : MonoBehaviour
{
    /// <summary>
    /// Indicates if the player finished the whole game
    /// </summary>
    public static bool GameCompleted
    {
        get;

        set;
    }

    private void Start()
    {
        GameCompleted = !GameCompleted;
    }
    public void EndAnimation()
    {
        StartCoroutine(NavMatrixMain.LoadSceneAsync(Convert.ToInt32(SceneOrders.MAIN_MENU), false));
    }
}
