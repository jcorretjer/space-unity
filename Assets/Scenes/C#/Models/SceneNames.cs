﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scenes.C_.Models
{
    public struct SceneNames
    {
        public const string AMARILLO = "Amarillo",
            CHINITA = "Chinita",
            VERDE = "Verde",
            AZUL = "Azul",
            INDIGO = "Indigo",
            VIOLETA = "Violeta",
            RED = "One";
    }
}
