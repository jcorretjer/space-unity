﻿using Assets.Scenes.C_.Models;
using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SplashScreenMain : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    void Start()
    {
        AudioSource audio = GetComponent<AudioSource>();

        LoadMainMenu(Convert.ToInt32(audio.clip.length));
    }

    private async void LoadMainMenu(int waitDuration)
    {
        await Task.Delay(waitDuration * 1000);

        //This runs on a separte thread so will run even if the scene changes. Don't load scene if it's not splash screen
        if(SceneManager.GetActiveScene().buildIndex == Convert.ToInt32(Game.SceneOrders.SPLASH_SCREEN))
            SceneManager.LoadScene(Convert.ToInt32(Game.SceneOrders.MAIN_MENU));
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        SceneManager.LoadScene(Convert.ToInt32(Game.SceneOrders.MAIN_MENU));
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        
    }
}
