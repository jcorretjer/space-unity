﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scenes.OneDirection.Combat.C_.Characters.Models
{
    public struct ConstantSpeeds
    {
        public const int LVL_1 = 2,
            LVL_2 = 4,
            LVL_3 = 6,
            LVL_4 = 6,
            LVL_5 = 6,
            LVL_6 = 8,
            LVL_7 = 10;
    }
}
