﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class FakeToggleSlider : MonoBehaviour, IPointerDownHandler
{
    private Slider slider;

    void Start()
    {
        slider = GetComponent<Slider>();
    }

    /// <summary>
    /// This works like the android switch. If it was clicked on max value goes to min val and vice versa.
    /// </summary>
    public void OnPointerDown(PointerEventData eventData)
    {
        if (slider.value > 0)
            slider.value = slider.minValue;

        else if (slider.value == 0)
            slider.value = slider.maxValue;

        //Set global audio based on the slider that was clicked
        if (gameObject.name.Contains("Music"))
        {
            CamContainerMain.GetGameClassObj().IsMusicOn = slider.value > 0;

            CamContainerMain.ToggleMusic(CamContainerMain.GetGameClassObj().IsMusicOn);
        }

        else
            CamContainerMain.GetGameClassObj().IsFXOn = slider.value > 0;

        if (CamContainerMain.GetGameClassObj().IsFXOn)
            GameObject.Find("CamContainer").GetComponents<AudioSource>()[Convert.ToInt32(CamContainerMain.CombatAudioSources.BTN_CLICK)].Play();
    }
}
