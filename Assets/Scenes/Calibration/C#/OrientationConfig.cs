﻿using Assets.Scenes.C_.Models;
using UnityEngine;

public class OrientationConfig : MonoBehaviour
{
    private void Start()
    {
        if (Time.timeScale < 1)
            Time.timeScale = 1;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Contains("Marker"))
        {
            Game mainGame = CamContainerMain.GetGameClassObj();

            /*
             * It's not changing currentGyro because the change was to be unnoticesable. If current was changed before combat started,
             * it would have rotated before going into combat and that;s not what we wanted
             * The best way was to store that value in a temp variable and before combat started spawning enemies, set currentGyro there.
             */
            if(gameObject.tag.ToUpper().Equals(Axi.Names.BACK.ToString()))
                mainGame.TempGyroInfo = new GyroInfo(Axi.Names.BACK);

            else if (gameObject.tag.ToUpper().Equals(Axi.Names.LEFT.ToString()))
                mainGame.TempGyroInfo = new GyroInfo(Axi.Names.LEFT);

            else if (gameObject.tag.ToUpper().Equals(Axi.Names.RIGHT.ToString()))
                mainGame.TempGyroInfo = new GyroInfo(Axi.Names.RIGHT);

            else if (gameObject.tag.ToUpper().Equals(Axi.Names.BOT.ToString()))
                mainGame.TempGyroInfo = new GyroInfo(Axi.Names.BOT);

            else if (gameObject.tag.ToUpper().Equals(Axi.Names.TOP.ToString()))
                mainGame.TempGyroInfo = new GyroInfo(Axi.Names.TOP);

            else
                mainGame.TempGyroInfo = new GyroInfo(Axi.Names.FRONT);

            //OrientationMain.DisplayAxisOrientation(gameObject.tag);

            Spawner.DestroyObject(other.gameObject);

            ConfirmationMain.ToggleConfirmation();
        }
    }
}
