﻿using Assets.Scenes.OneDirection.Combat.C_.Models;
using UnityEngine;
using Random = System.Random;

namespace Assets.Scenes.OneDirection.Combat.C_.Characters.Models
{
    public class Character
    {
        /// <summary>
        /// It's considered to be dead if it's 1 or below because the hp bar is so thin you can't really see it. This is to avoid player confusion
        /// </summary>
        public const int CONSIDERED_DEAD = 1;

        public Character()
        {
            maxHP = 100;

            currentHP = maxHP;

            dmgPts = 0;

            randomSpawn = new Random();
        }

        public enum Type
        {
            PLAYER,
            ENEMY
        }

        public Type type
        {
            get;

            set;
        }

        public float maxHP
        {
            get;

            set;
        }

        public float currentHP
        {
            get;

            set;
        }

        public float dmgPts
        {
            get;

            set;
        }

        public Movement movement
        {
            get;

            set;
        }

        /// <summary>
        /// This will calculate the current hp after the dmg that was recieved and
        /// set the x scale of the hp bar based on on the current hp value.
        /// </summary>
        /// <param name="dmgReceived"></param>
        /// <param name="yVal">Hpbar Y scale so that it satys the same value</param>
        /// <returns>The new scale for the hp</returns>
        public Vector3 CalculateHPBar(float dmgReceived, float yVal)
        {
            currentHP -= dmgReceived;

            return new Vector3(currentHP / maxHP, yVal, 1);
        }

        /// <summary>
        /// This will calculate the current hp based on the current hp and
        /// set the x scale of the hp bar based on on the current hp value.
        /// </summary>
        /// <param name="dmgReceived"></param>
        /// <param name="yVal">Hpbar Y scale so that it satys the same value</param>
        /// <returns>The new scale for the hp</returns>
        public Vector3 CalculateHPBar(float yVal)
        {
            return new Vector3(currentHP / maxHP, yVal, 1);
        }

        public float FindQuarterHPValue()
        {
            return maxHP * 0.25f;
        }

        private static Random randomSpawn;

        /// <summary>
        /// Based on the character type and the specific data for the level, 
        /// it will generate a vector 3 with random X and Y values. Z is always the same value.
        /// </summary>
        /// <param name="charType"></param>
        /// <param name="lVLSpecific"></param>
        /// <returns></returns>
        public static Vector3 GenerateRandomSpawn(Type charType, LVLSpecific lVLSpecific)
        {
            switch (charType)
            {
                case Type.ENEMY:

                    return new Vector3(randomSpawn.Next(lVLSpecific.MinX, lVLSpecific.MaxX), randomSpawn.Next(lVLSpecific.MinY, lVLSpecific.MaxY), lVLSpecific.MaxZ);

                default:

                    return new Vector3();
            }
        }
    }
}
