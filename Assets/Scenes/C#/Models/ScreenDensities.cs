﻿
namespace Assets.Scenes.C_.Models
{
    public struct ScreenResolutions
    {
        public const int S2160 = 2160, //S= screen
            S2270 = 2270,
            S2560 = 2560,
            S1920 = 1920,
            S2960 = 2960;
    }
}
