﻿using UnityEngine;

public class Explosion : MonoBehaviour
{
    private float minForce = 105f,
        maxForce = 400f,
        radius = 10f;

    private static GameObject _gameObj;

    private static bool _justDied = false, // Used to control the update. 
        _addForce = true; // Used to control the force pushing.

    /// <summary>
    /// Used to start the death animation.
    /// 
    /// This only starts once per call.
    /// </summary>
    /// <param name="gameObj">The enemy that should play death animation</param>
    /// <param name="addForce"></param>
    public static void SetJustDied(GameObject gameObj, bool addForce)
    {
        _justDied = true;

        _gameObj = gameObj;

        _addForce = addForce;
    }    

    private void Update()
    {
        if (_justDied)
        {
            _justDied = false;

            //Grabs every child gameObj pulls it's rigidbody and adds force to make it look like it's exploting 
            foreach (Transform t in _gameObj.transform)
            {
                Rigidbody rb = t.GetComponent<Rigidbody>();

                if (rb != null)
                {
                    /*
                     * I had to disable the render because they rotate and slowly push themselve out of the enemy container.
                     * 
                     * To make it look more realistic, the renderer enables only when it hits something
                     */
                    t.GetComponent<MeshRenderer>().enabled = true;

                    t.GetComponent<EnamyShardMain>().StartFadeout(t.gameObject);

                    if(_addForce)
                        rb.AddExplosionForce(Random.Range(minForce, maxForce), transform.position, radius);
                }                
            }
        }
    }
}
