﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransEndingMain : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //Disable it because the previous scene wasn't credits
        if (!EndingCreditsMain.GameCompleted)
            gameObject.SetActive(!gameObject.activeSelf);
    }

    public void EndAnimation()
    {
        EndingCreditsMain.GameCompleted = !EndingCreditsMain.GameCompleted;

        gameObject.SetActive(!gameObject.activeSelf);
    }
}
