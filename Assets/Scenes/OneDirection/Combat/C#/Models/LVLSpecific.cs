﻿using Assets.Scenes.C_.Models;
using Assets.Scenes.OneDirection.Combat.C_.Characters.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Assets.Scenes.C_.Models.Game;

namespace Assets.Scenes.OneDirection.Combat.C_.Models
{
    public class LVLSpecific
    {
        public float ConstantSpeed
        {
            get;

            set;
        }

        public int MaxTotalSpawn
        {
            get;

            set;
        }

        public int MinX
        {
            get;

            set;
        }

        public int MinY
        {
            get;

            set;
        }

        public int MaxX
        {
            get;

            set;
        }

        public int MaxY
        {
            get;

            set;
        }

        public int MaxZ
        {
            get;

            set;
        }

        public SceneOrders NextScene
        {
            get;

            set;
        }

        /// <summary>
        /// Get all specific data for the currently playing level.
        /// </summary>
        /// <param name="lvlName"></param>
        /// <returns></returns>
        public LVLSpecific GetLVLSpecificByLvlName(string lvlName)
        {
            int minX = 0,
                minY = 0,
                maxX = 0,
                maxY = 0,
                maxZ = 0,
                maxTotalSpawn = 0;

            float constantSpeed = 0;

            SceneOrders sceneOrder = SceneOrders.ONE_DIRECTION;

            if (lvlName.Contains(SceneNames.AMARILLO))
            {
                constantSpeed = ConstantSpeeds.LVL_3;

                minX = SpawnPoints.MIN_X_3;

                minY = SpawnPoints.MIN_Y_3;

                maxX = SpawnPoints.MAX_X_3;

                maxY = SpawnPoints.MAX_Y_3;

                maxZ = SpawnPoints.MAX_Z_3;

                maxTotalSpawn = MaxTotalSpawnAmounts.LVL_3;

                sceneOrder = SceneOrders.VERDE;
            }

            else if (lvlName.Contains(SceneNames.AZUL))
            {
                constantSpeed = ConstantSpeeds.LVL_5;

                minX = SpawnPoints.MIN_X_5;

                minY = SpawnPoints.MIN_Y_5;

                maxX = SpawnPoints.MAX_X_5;

                maxY = SpawnPoints.MAX_Y_5;

                maxZ = SpawnPoints.MAX_Z_5;

                maxTotalSpawn = MaxTotalSpawnAmounts.LVL_5;

                sceneOrder = SceneOrders.INDIGO;
            }

            else if (lvlName.Contains(SceneNames.CHINITA))
            {
                constantSpeed = ConstantSpeeds.LVL_2;

                minX = SpawnPoints.MIN_X_2;

                minY = SpawnPoints.MIN_Y_2;

                maxX = SpawnPoints.MAX_X_2;

                maxY = SpawnPoints.MAX_Y_2;

                maxZ = SpawnPoints.MAX_Z_2;

                maxTotalSpawn = MaxTotalSpawnAmounts.LVL_2;

                sceneOrder = SceneOrders.AMARILLO;
            }

            else if (lvlName.Contains(SceneNames.INDIGO))
            {
                constantSpeed = ConstantSpeeds.LVL_6;

                minX = SpawnPoints.MIN_X_6;

                minY = SpawnPoints.MIN_Y_6;

                maxX = SpawnPoints.MAX_X_6;

                maxY = SpawnPoints.MAX_Y_6;

                maxZ = SpawnPoints.MAX_Z_6;

                maxTotalSpawn = MaxTotalSpawnAmounts.LVL_6;

                sceneOrder = SceneOrders.VIOLETA;
            }

            else if (lvlName.Contains(SceneNames.VERDE))
            {
                constantSpeed = ConstantSpeeds.LVL_4;

                minX = SpawnPoints.MIN_X_4;

                minY = SpawnPoints.MIN_Y_4;

                maxX = SpawnPoints.MAX_X_4;

                maxY = SpawnPoints.MAX_Y_4;

                maxZ = SpawnPoints.MAX_Z_4;

                maxTotalSpawn = MaxTotalSpawnAmounts.LVL_4;

                sceneOrder = SceneOrders.AZUL;
            }

            else if (lvlName.Contains(SceneNames.VIOLETA))
            {
                constantSpeed = ConstantSpeeds.LVL_7;

                minX = SpawnPoints.MIN_X_7;

                minY = SpawnPoints.MIN_Y_7;

                maxX = SpawnPoints.MAX_X_7;

                maxY = SpawnPoints.MAX_Y_7;

                maxZ = SpawnPoints.MAX_Z_7;

                maxTotalSpawn = MaxTotalSpawnAmounts.LVL_7;

                sceneOrder = SceneOrders.CREDITS;
            }

            else
            {
                constantSpeed = ConstantSpeeds.LVL_1;

                minX = SpawnPoints.MIN_X;

                minY = SpawnPoints.MIN_Y;

                maxX = SpawnPoints.MAX_X;

                maxY = SpawnPoints.MAX_Y;

                maxZ = SpawnPoints.MAX_Z;

                maxTotalSpawn = MaxTotalSpawnAmounts.LVL_1;

                sceneOrder = SceneOrders.CHINITA;
            }

            return new LVLSpecific()
            {
                ConstantSpeed = constantSpeed,

                MinX = minX,

                MaxTotalSpawn = maxTotalSpawn,

                MaxX = maxX,

                MaxY = maxY,

                MaxZ = maxZ,

                MinY = minY,

                NextScene = sceneOrder
            };
        }
    }
}
