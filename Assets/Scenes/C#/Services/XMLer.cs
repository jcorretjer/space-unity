﻿using Assets.Scenes.C_.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Assets.Scenes.C_.Services
{
    public static class XMLer
    {
        public static string Serialize<T>(this T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter stringWriter = new StringWriter())
            {
                xmlSerializer.Serialize(stringWriter, toSerialize);

                return stringWriter.ToString();
            }
        }

        public static T Deserialize<T>(this string serialized)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Unlocks));

            using (StringReader reader = new StringReader(serialized))
            {
                return (T)xmlSerializer.Deserialize(reader);
            }
        }
    }
}
