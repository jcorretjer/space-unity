﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scenes.C_.Models
{
    public class GyroInfo
    {
        public Gyroscope gyroscope
        {
            get;

            set;
        }

        public GyroInfo(Axi.Names axiName)
        {
            Orientation = Axi.GetOrientation(axiName);
        }

        public GyroInfo()
        {
            Orientation = Axi.GetOrientation(Axi.Names.FRONT);
        }

        public Coord Orientation
        {
            get;

            set;
        }
    }
}
