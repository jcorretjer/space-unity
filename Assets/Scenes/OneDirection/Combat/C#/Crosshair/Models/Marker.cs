﻿using UnityEngine;

namespace Assets.Scenes.OneDirection.Combat.C_.Crosshair.Models
{
    public class Marker
    {
        public const int LIFE_TIME = 800,
        CONSTANT_SPEED = 105;

        public static Vector3 InitialMarkerPos = new Vector3(0.11f, -0.03f, 1f);

        public Marker()
        {

        }
    }
}
