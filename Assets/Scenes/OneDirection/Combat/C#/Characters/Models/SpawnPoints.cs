﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scenes.OneDirection.Combat.C_.Characters.Models
{
    /// <summary>
    /// Contains max spawn values for a character.
    /// </summary>
    public struct SpawnPoints
    {
        public const int MAX_X = 20,
            MIN_X = -20,
            MAX_X_2 = 60,
            MIN_X_2 = -60,
            MAX_X_3 = 70,
            MIN_X_3 = -70,
            MAX_X_4 = 70,
            MIN_X_4 = -70,
            MAX_X_5 = 80,
            MIN_X_5 = -80,
            MAX_X_6 = 90,
            MIN_X_6 = -90,
            MAX_X_7 = 100,
            MIN_X_7 = -100,

            MAX_Y = 41,
            MIN_Y = -16,
            MAX_Y_2 = 51,
            MIN_Y_2 = -16,
            MAX_Y_3 = 61,
            MIN_Y_3 = -16,
            MAX_Y_4 = 71,
            MIN_Y_4 = -26,
            MAX_Y_5 = 71,
            MIN_Y_5 = -26,
            MAX_Y_6 = 91,
            MIN_Y_6 = -36,
            MAX_Y_7 = 101,
            MIN_Y_7 = -36,

            MAX_Z = 30,
            MAX_Z_2 = 36,
            MAX_Z_3 = 42,
            MAX_Z_4 = 48,
            MAX_Z_5 = 54,
            MAX_Z_6 = 60,
            MAX_Z_7 = 70;
    }
}
