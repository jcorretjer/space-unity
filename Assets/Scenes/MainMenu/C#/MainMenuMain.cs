﻿using Assets.Scenes.C_.Models;
using System;
using System.Collections;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using static Assets.Scenes.C_.Models.Game;

public class MainMenuMain : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    private GameObject navMatrixPan,
        lvlModePan,
        lvlPan,
        modePan,
        transEndingImg,
        chinitaBtn,
        amarilloBtn,
        verderBtn,
        azulBtn,
        indigoBtn,
        violetaBtn;

    private Game mainGame;

    public enum AudioSrcs
    {
        BTN_CLICK = 0,

        LVL_SONG
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!mainGame.Testing)
        {
            // Don't allow to get past the main menu if not supported
            if (mainGame.SupportsGyro)
            {
                if (CamContainerMain.GetGameClassObj() != null)
                {
                    if (CamContainerMain.GetGameClassObj().IsFXOn)
                        GameObject.Find("MainCam").GetComponent<AudioSource>().Play();
                }

                else
                    GameObject.Find("MainCam").GetComponent<AudioSource>().Play();

                PlayClickedFX();

                gameObject.SetActive(navMatrixPan.activeSelf);

                lvlModePan.SetActive(!gameObject.activeSelf);

                //navMatrixPan.SetActive(!gameObject.activeSelf);

                //NavMatrixMain.shouldLoadScene = true;
            }
        }

        else
        {
            PlayClickedFX();

            gameObject.SetActive(navMatrixPan.activeSelf);

            lvlModePan.SetActive(!gameObject.activeSelf);
        }
}

    GameObject noGyroSupportImg;

    private void Awake()
    {
        if (CamContainerMain.GetGameClassObj() == null)
        {
            mainGame = new Game()
            {
                SupportsGyro = SystemInfo.supportsGyroscope
            };

            CamContainerMain.LoadPrivateData(mainGame, out mainGame);
        }

        else
            mainGame = CamContainerMain.GetGameClassObj();

        if (!mainGame.Testing)
        {
            // Make no support msg invisible. UNCOMMENT THIS WHOLE IF, WHEN THE TESTING IS DONE.
            if (SystemInfo.supportsGyroscope)
                GameObject.Find("NoGyroSupportImg").SetActive(!GameObject.Find("NoGyroSupportImg").activeSelf);

            // Make no support msg invisible. COMMENT/DELETE THIS WHOLE IF, WHEN THE TESTING IS DONE.
            //if (SystemInfo.supportsGyroscope)
            //{
            //    //Test period ends 12/16/2019. After that they won't be able to test.
            //    if (DateTime.Now.Date < DateTime.Parse("12/18/2019"))
            //        GameObject.Find("NoGyroSupportImg").SetActive(!GameObject.Find("NoGyroSupportImg").activeSelf);

            //    else
            //        GameObject.Find("NoSupportLbl").GetComponent<TextMeshProUGUI>().text = "Unfortunately, SPACE cannot be played because the testing period has ended.";
            //}
        }

        else
            GameObject.Find("NoGyroSupportImg").SetActive(!GameObject.Find("NoGyroSupportImg").activeSelf);
    }

    void Start()
    {
        #region Disabled pans

        navMatrixPan = GameObject.Find("NavMatrixPan");

        navMatrixPan.SetActive(!gameObject.activeSelf);

        #region Lvl select btns

        chinitaBtn = GameObject.Find("ChinitaBtn");

        chinitaBtn.SetActive(mainGame.Unlocks.Chinita);

        amarilloBtn = GameObject.Find("AmarilloBtn");

        amarilloBtn.SetActive(mainGame.Unlocks.Amarillo);

        verderBtn = GameObject.Find("VerdeBtn");

        verderBtn.SetActive(mainGame.Unlocks.Verde);

        azulBtn = GameObject.Find("AzulBtn");

        azulBtn.SetActive(mainGame.Unlocks.Azul);

        indigoBtn = GameObject.Find("IndigoBtn");

        indigoBtn.SetActive(mainGame.Unlocks.Indigo);

        violetaBtn = GameObject.Find("VioletaBtn");

        violetaBtn.SetActive(mainGame.Unlocks.Violeta); 

        #endregion

        lvlPan = GameObject.Find("LvlPan");

        lvlPan.SetActive(!gameObject.activeSelf);

        modePan = GameObject.Find("ModePan");

        lvlModePan = GameObject.Find("LvlModePan");

        lvlModePan.SetActive(!gameObject.activeSelf);

        #endregion

        if (CamContainerMain.GetGameClassObj() != null && !CamContainerMain.GetGameClassObj().IsMusicOn)
            GameObject.Find("MainCam").GetComponents<AudioSource>()[Convert.ToInt32(AudioSrcs.LVL_SONG)].Stop();

        TextMeshProUGUI text = GameObject.Find("BuildNumLbl").GetComponent<TextMeshProUGUI>();

        text.text = string.Format("{0} {1}", text.text, Application.version);
    }

    public void StoryBtnClicked()
    {
        PrepCombat(mainGame.LevelSelected, Modes.STORY);
    }

    public void DisplayNavMatrix()
    {
        navMatrixPan.SetActive(!gameObject.activeSelf);

        NavMatrixMain.shouldLoadScene = true;
    }

    public void EndlessBtnClicked()
    {
        PlayClickedFX();

        modePan.SetActive(!modePan.activeSelf);

        lvlPan.SetActive(!lvlPan.activeSelf);

        mainGame.Mode = Modes.ENDLESS;
    }

    public void BackBtnClicked()
    {
        EndlessBtnClicked();
    }

    private void PlayClickedFX()
    {
        if (CamContainerMain.GetGameClassObj() != null)
        {
            if (CamContainerMain.GetGameClassObj().IsFXOn)
                GameObject.Find("MainCam").GetComponent<AudioSource>().Play();
        }

        else
            GameObject.Find("MainCam").GetComponent<AudioSource>().Play();
    }

    private void PrepCombat(SceneOrders lvlSelected, Modes mode)
    {
        PlayClickedFX();

        DisplayNavMatrix();

        mainGame.LevelSelected = lvlSelected;

        mainGame.Mode = mode;

        if(CamContainerMain.GetGameClassObj() != null)
        {
            Game activeMainGame = CamContainerMain.GetGameClassObj();

            activeMainGame.Mode = mainGame.Mode;

            activeMainGame.LevelSelected = mainGame.LevelSelected;
        }

        CamContainerMain.SetGameClassObj(mainGame);
    }

    public void RojoClicked()
    {
        PrepCombat(SceneOrders.ONE_DIRECTION, Modes.ENDLESS);
    }

    public void ChinitaClicked()
    {
        PrepCombat(SceneOrders.CHINITA, Modes.ENDLESS);
    }

    public void AmarilloClicked()
    {
        PrepCombat(SceneOrders.AMARILLO, Modes.ENDLESS);
    }

    public void VerdeClicked()
    {
        PrepCombat(SceneOrders.VERDE, Modes.ENDLESS);
    }

    public void AzulClicked()
    {
        PrepCombat(SceneOrders.AZUL, Modes.ENDLESS);
    }

    public void IndigoClicked()
    {
        PrepCombat(SceneOrders.INDIGO, Modes.ENDLESS);
    }

    public void VioletaClicked()
    {
        PrepCombat(SceneOrders.VIOLETA, Modes.ENDLESS);
    }
    
}
