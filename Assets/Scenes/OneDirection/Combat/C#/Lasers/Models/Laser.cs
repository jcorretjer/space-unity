﻿using UnityEngine;

namespace Assets.Scenes.OneDirection.Combat.C_.Lasers.Models
{
    public class Laser
    {
        public const float ConstantSpeed = 125f;

        public Vector3 InitialLeftPos = new Vector3(0, -5.37f, 0);

        public Laser()
        {
            ShouldMove = true;
        }

        public bool ShouldMove
        {
            get;

            set;
        }
    }
}
