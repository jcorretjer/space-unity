﻿using Assets.Scenes.OneDirection.Combat.C_.Characters.Models;
using Assets.Scenes.OneDirection.Combat.C_.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Unity.Jobs;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public static GameObject markerPrefab;

    public GameObject _markerPrefab; // For some reason I think some values were null if I set this to static. So I create this as a regular and assign it to it's static version

    public static GameObject laserPrefab;

    public GameObject _laserPrefab;

    public static GameObject enemyPrefab;

    public GameObject _enemyPrefab;

    //Series of counters used as ID for each gameobj
    private static int laserIDCount = 0,
        enemyIDCount = 0,
        markerIDCount = 0;
    
    /// <summary>
    /// It's in spawner because every enemy should use this at the same time
    /// </summary>
    public static bool ShouldEnemiesMove
    {
        get;

        set;
    }

    private static Dictionary<string, Transform> enemiesMarked = new Dictionary<string, Transform>();

    /// <summary>
    /// Get the coordinates of the marked enemy.
    /// 
    /// This is used to guide the laser
    /// </summary>
    /// <param name="markerName"></param>
    /// <returns></returns>
    public static Transform GetEnemyMarked(string markerName)
    {       
        return enemiesMarked.ContainsKey(markerName) 
            ? enemiesMarked[markerName]
            : null;
    }

    /// <summary>
    /// Once an enemy is marked add it to the list. That way the laser can use this to guide itself
    /// </summary>
    /// <param name="markerName"></param>
    /// <param name="enemyTransform"></param>
    public static void AddMarkedEnemy(string markerName, Transform enemyTransform)
    {
        enemiesMarked.Add(markerName, enemyTransform);
    }

    void Start()
    {
        markerPrefab = _markerPrefab;

        laserPrefab = _laserPrefab;

        /*
         * It's going to be null if it's on calibration because it tries to pull the enemy prefab that's being set from the editor. This is only set on combat. 
         * Calibration runs most of the things here but skips everything related to enemies
         */
        enemyPrefab = _enemyPrefab == null 
            ? new GameObject() 
            : _enemyPrefab;
    }

    /// <summary>
    /// Adds the count(whish is like the marker ID) to the name. That way we can identify it.
    /// And instantiates it make it exist.
    /// 
    /// Each marker has a laser. So if the marker ID is 1 then it's brother laser is 1
    /// </summary>
    public static void SpawnMarker()
    {
        markerPrefab.name = string.Format("CrossMarker-{0}-", markerIDCount);

        Instantiate(markerPrefab, Main.GetCrosshair().transform.position, Main.GetCrosshair().transform.rotation);

        markerIDCount++;
    }

    /// <summary>
    /// Same as SpawnMarker, but for the laser.
    /// 
    /// Each marker has a laser. So if the marker ID is 1 then it's brother laser is 1
    /// </summary>
    public static void SpawnLaser()
    {
        laserPrefab.name = string.Format("Laser-{0}-", laserIDCount);

        Instantiate(laserPrefab);

        laserIDCount++;
    }

    /// <summary>
    /// Used to count how manay enemies have spawned. Once it reaches the querter count, resets to 0.
    /// This way we can determine when an enemy should be speed boosted
    /// </summary>
    public static int spawnCounter = 0;

    public static int GetSpawnCounter()
    {
        return spawnCounter;
    }

    public static void SetSpawnCounter(int num)
    {
        spawnCounter = num;
    }

    /// <summary>
    /// Same as SpawnMarker, but for the laser. With a few adjustments
    ///
    /// </summary>
    private static void SpawnEnemy()
    {
        /*
         * Sometimes when it switches to calibration it's in the middle of spawning an enemy. 
         * At that very moment, the enemy game object is a new instance of game object which is not what it's looking for.
         * The name of a new game object starts with 'New'. Skip spawning an enemy of it's an empty object.
         * Por alguna razon en cualquier otro scene que no sea el One direction, esto corre primero que el start. Por eso se verifica para null
         */
        if (enemyPrefab != null && !enemyPrefab.name.Contains("New"))
        {
            enemyPrefab.name = string.Format("EnemyContainer-{0}-", enemyIDCount);

            enemyPrefab.transform.position = Character.GenerateRandomSpawn(Character.Type.ENEMY, CamContainerMain.GetSpwn().LVLSpecific);

            Instantiate(enemyPrefab);

            if(CamContainerMain.GetGameClassObj().Mode == Assets.Scenes.C_.Models.Game.Modes.ENDLESS)
            {
                //Means that it reached the end of the current phase
                if((enemyIDCount + 1) == currentMaxAmount)
                {
                    Spwn spwn = CamContainerMain.GetSpwn();

                    //Change the current lvl data to the next lvl
                    if (spwn.LVLSpecific.NextScene != Assets.Scenes.C_.Models.Game.SceneOrders.VIOLETA)
                    {
                        spwn = CamContainerMain.SetSpwn(new Spwn(System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(spwn.LVLSpecific.NextScene.ToString().ToLower())));

                        currentMaxAmount += spwn.LVLSpecific.MaxTotalSpawn;
                    }

                    //Reached the last existing lvl phase just keep creating more phases with 6 enemies each
                    else
                        currentMaxAmount += 6;

                    //This will reach it's peak speed I think it was 2 phases after the last lvl existing phase which is Just what we wanted 
                    if(currentSpawnDelay > Spwn.CLIMAX_SPAWN_TIME)
                    {
                        currentSpawnDelay -= Spwn.ENDLESS_SPAWN_TIME_SUBSTRACTION;
                    }
                }
            }

            else
            {
                spawnCounter++;

                enemiesAliveCount++;

                bool stopDeclineSubtraction = false;

                //*-1 means we reached climax point and time descend the slope
                if (currentSpawnDelay <= Spwn.CLIMAX_SPAWN_TIME)
                {
                    spawnTimeSubstractor *= -1;

                    stopDeclineSubtraction = currentSpawnDelay < Spwn.ENDING_SPAWN_TIME;
                }

                if (!stopDeclineSubtraction)
                    currentSpawnDelay -= Convert.ToInt32(spawnTimeSubstractor);
            }
        }
    }

    private static int currentSpawnDelay;

    public static int enemiesAliveCount = 0;

    private static float spawnTimeSubstractor;

    private static Spwn spwn;

    private static int oldMaxAmount, //Keep the oldAmount just in case we have two asyncs running. The new one and the previous one. By using this we can stop the previous one
        oldMaxEndlessAmount,
        currentMaxAmount;

    /// <summary>
    /// Start spawning enemies based on the randomly selected max amount of enemies, for that level,
    /// and the rate which will be the delay of the task.
    /// 
    ///<param name="maxAmount">Total spawn amount</param>
    /// </summary>
    public static async Task StartSpawningEnemies(int maxAmount)
    {
        currentMaxAmount = maxAmount;

        ShouldEnemiesMove = true;

        currentSpawnDelay = Spwn.STARTING_SPAWN_TIME;

        spwn = CamContainerMain.GetSpwn();

        spwn.CalcSpawnClimaxNEndIndicator();        

        spawnTimeSubstractor = spwn.CalcSpawnTimeSubstractor();

        //Por alguna razon esto aveces es mayor a 0 y causa que el juego no cambie de nivel cuando terminado
        if (enemyIDCount > 0)
            enemyIDCount = 0;
        
        for (; enemyIDCount < currentMaxAmount; enemyIDCount++)
        {

            /* 
             * No se como explicar por que pasa, pero cuando le das restart no se termina el async que esta corriendo. Se crea otro mas so hay dos corriendo a la vez. 
             * Pero el ordern que corren es primero corre el primero que se creo y luego el nuevo. Si el valor del old max es igual al nuevo quiere decir
             * que esta corriendo el viejo so break out of it para que solo esta el nuevo corriendo solo.
             * 
             * Cuando cambia de scene no salen dos por que el async llega a terminar antes que empieze el otro por que pasan muchas cosas antes que empieze el otro como lo del hyper speed
             * y entre ootras cosas. En el restart no hay nada de eso solo hace restart rapido y esto es lo primero que empieza por eso es que se crea un segundo async mietras esta el anterior.
             */
            if (CamContainerMain.GetGameClassObj().Mode == Assets.Scenes.C_.Models.Game.Modes.STORY)
            {
                if (maxAmount == oldMaxAmount)
                {
                    oldMaxAmount = 0;

                    break;
                }
            }

            else
            {
                /*
                 * For endless I don't use max amount because it can go beyond any of the harcoded max amounts. 
                 * 
                 * Just check the old amount and set it to 0 cause it's not used for anything else
                 */
                if (oldMaxAmount > 0)
                {
                    oldMaxAmount = 0;

                    break;
                }
            }

            //if (maxAmount == oldMaxAmount)
            //        break;

            //Stop spawning if paused or if reached max active enemies limit
            if (Time.timeScale > 0 && enemiesAliveCount < Spwn.MAX_ENEMIES_ALIVE_LIMIT)
                    SpawnEnemy();

                //-- so that the count stays the same when paused. If not, this woill cause eneimy IDs to be incorrect
                else
                    enemyIDCount--;

                await Task.Delay(currentSpawnDelay);
            }
    }

    /// <summary>
    /// The enemy spawner run in async so it means it's going to run even if the secen for it isn't loaded. 
    /// 
    /// Setting all variables to default values forces the enemy spawner to start clean, but does not stop the async loop.
    /// </summary>
    public static void ResetSpawning()
    {
        enemiesAliveCount = 0;

        enemyIDCount = 0;

        spawnCounter = 0;

        oldMaxAmount = currentMaxAmount;
    }

    /// <summary>
    /// The enemy spawner run in async so it means it's going to run even if the secen for it isn't loaded. 
    /// 
    /// Stops the async loop and then resets all values to default and ready for a clean start.
    /// </summary>
    public static void StopSpawningEnemies()
    {
        enemyIDCount = spwn.TotalAmount;

        ShouldEnemiesMove = false;

        ResetSpawning();
    }

    /// <summary>
    /// Invoke the native destroy method
    /// </summary>
    /// <param name="game">Item to destroy</param>
    public static void DestroyObject(GameObject game)
    {
        Destroy(game);
    }
}
