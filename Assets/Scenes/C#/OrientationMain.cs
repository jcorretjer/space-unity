﻿using UnityEngine;
using UnityEngine.UI;

public class OrientationMain : MonoBehaviour
{
    private static Text orientationLbl;

    void Start()
    {
        orientationLbl = GetComponent<Text>();
    }

    /// <summary>
    /// Only being used in the calibarion scene.
    /// 
    /// Just display where it's being hit
    /// </summary>
    /// <param name="axisName"></param>
    //public static void DisplayAxisOrientation(string axisName)
    //{
    //    orientationLbl.text = axisName;
    //}
}
