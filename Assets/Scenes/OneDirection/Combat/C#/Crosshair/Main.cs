﻿using Assets.Scenes.OneDirection.Combat.C_.Models;
using UnityEngine;

public class Main : MonoBehaviour
{
    private Gyroscope gyro;

    private static Crosshair crosshair;

    void Start()
    {      
        crosshair = new Crosshair()
        {
            Rigidbod = GetComponent<Rigidbody2D>()
        };
    }

    void Update()
    {
        crosshair.transform = transform;
    }

    public static Crosshair GetCrosshair()
    {
        return crosshair;
    }
}
