﻿using System;
using UnityEngine;
using static CamContainerMain;

public class CrackedScreenMain : MonoBehaviour
{
    private static Animator animator;


    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public static void StartHitAnimation()
    {
        if(GetGameClassObj().IsFXOn)
            GameObject.Find("CamContainer").GetComponents<AudioSource>()[Convert.ToInt32(CombatAudioSources.SCREEN_HIT)].Play();

        animator.SetTrigger("wasHit");
    }

    public void EndHitAnimation()
    {
        animator.SetTrigger("endHit");
    }
}
