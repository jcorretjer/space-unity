﻿using Assets.Scenes.C_.Models;
using Assets.Scenes.OneDirection.Combat.C_.Models;
using System;
using UnityEngine;

public class HyperSpaceMain : MonoBehaviour
{
    public static bool AnimationCompleted
    {
        get;

        set;
    }

    private void Start()
    {
        AnimationCompleted = false;
    }

    /// <summary>
    /// Executes all the necessary lines to run the blue hyperspaces transition animation.
    /// 
    /// 1st runs the orange animation and this ones follows right after. 
    /// They almost start at the same time. Once the orange runs it invokes this one to start the blue
    /// </summary>
    public void PlayBlue()
    {
        if(CamContainerMain.GetGameClassObj().IsFXOn)
            GetComponent<AudioSource>().Play();

        CamContainerMain.ToggleHUDInteractions(false);

        StartCoroutine(Game.FadeOutMusic(CamContainerMain.GetCurrentAudioSrc()));

        GameObject.Find("Blue").GetComponent<Animator>().SetTrigger("ChangeLvl");

        StartCoroutine(Game.SetSkyboxExposure(RenderSettings.skybox, 0, Time.deltaTime * 2));

        #region Handle lvl unlocks

        Game game = CamContainerMain.GetGameClassObj();

        LVLSpecific lVLSpecific = CamContainerMain.GetSpwn().LVLSpecific;

        switch (lVLSpecific.NextScene)
        {
            case Game.SceneOrders.AMARILLO:

                game.Unlocks.Amarillo = true;

                break;

            case Game.SceneOrders.AZUL:

                game.Unlocks.Azul = true;

                break;

            case Game.SceneOrders.CHINITA:

                game.Unlocks.Chinita = true;

                break;

            case Game.SceneOrders.INDIGO:

                game.Unlocks.Indigo = true;

                break;

            case Game.SceneOrders.VERDE:

                game.Unlocks.Verde = true;

                break;

            case Game.SceneOrders.VIOLETA:

                game.Unlocks.Violeta = true;

                break;
        }

        CamContainerMain.SetGameClassObj(game); 

        #endregion

        StartCoroutine(NavMatrixMain.LoadSceneAsyncAfterHyperSpace(Convert.ToInt32(lVLSpecific.NextScene)));

        //StartCoroutine(NavMatrixMain.LoadSceneAsyncAfterHyperSpace(Convert.ToInt32(CamContainerMain.GetSpwn().LVLSpecific.NextScene)));
    }

    /// <summary>
    /// Called once the blue hyperspace is done. It's an event
    /// </summary>
    public void CompleteAnimation()
    {
        AnimationCompleted = true;
    }
}
