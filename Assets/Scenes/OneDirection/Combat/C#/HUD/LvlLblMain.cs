﻿using System.Threading.Tasks;
using UnityEngine;

public class LvlLblMain : MonoBehaviour
{
    private static Animator animator;

    /// <summary>
    /// Start animation after 2 seconds. Do it async so it doesn't freze the UI thread.
    /// </summary>
    public static async void PlayAnimationAfterDelay()
    {
        animator = GameObject.Find("LvlLbl" + CamContainerMain.GetGameClassObj().CurrentScreenRes).GetComponent<Animator>();

        await Task.Delay(2000);

        StartAnimation();
    }

    private static void StartAnimation()
    {
        animator.SetBool("displayLvlName", true);
    }
}
