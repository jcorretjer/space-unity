﻿
namespace Assets.Scenes.C_.Models
{
    public class Axi

    {
        public enum Names
        {
            FRONT,
            TOP,
            BOT,
            RIGHT,
            LEFT,
            BACK
        }

        /// <summary>
        /// Get the orientation vector 3 coordinates based on the angle name
        /// </summary>
        /// <param name="axiName">Angle name</param>
        /// <returns>Coord obj that can be used to initiate any vector 3 or anything similar</returns>
        public static Coord GetOrientation(Names axiName)
        {
            Coord coord;

            switch (axiName)
            {
                case Names.TOP:

                   coord = new Coord()
                   {
                       x = 172f,

                       y = -5f,

                       z = -182f
                   };

                    break;

                case Names.BACK:

                    coord = new Coord()
                    {
                        x = 72f,

                        y = -5f,

                        z = 0f
                    };

                    break;

                case Names.LEFT:

                    coord = new Coord()
                    {
                        x = 72f,

                        y = -5f,

                        z = 82f
                    };

                    break;

                case Names.RIGHT:

                    coord = new Coord()
                    {
                        x = 72f,

                        y = -5f,

                        z = -364f
                    };

                    break;

                case Names.BOT:

                    coord = new Coord()
                    {
                        x = 0f,

                        y = -5f,

                        z = -182f
                    };

                    break;

                default:

                    coord = new Coord()
                    {
                        x = 72f,

                        y = -5f,

                        z = -182f
                    };

                    break;

            }

            return coord;
        }

    }
}
