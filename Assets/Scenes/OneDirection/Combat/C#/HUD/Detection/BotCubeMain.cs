﻿using Assets.Scenes.C_.Models;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BotCubeMain : MonoBehaviour
{
    private static TextMeshProUGUI botLbl;

    public static void Init()
    {
        botLbl = GameObject.Find("DetectionBotLbl").GetComponent<TextMeshProUGUI>();        

        botLbl.enabled = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals(Tags.ENEMY))
            botLbl.enabled = true;

        else if (other.tag.Equals(Tags.EXPLODING_ENEMY))
        {
            int childCount = other.transform.root.gameObject.GetComponentInChildren<Transform>().childCount;

            //Means that the enemy is on death animation
            if(childCount < 2)
                botLbl.enabled = false;
        }

        //Means that the nothing is colliding except the other cubes
        else
            botLbl.enabled = false;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals(Tags.ENEMY))
            botLbl.enabled = false;
    }
}
