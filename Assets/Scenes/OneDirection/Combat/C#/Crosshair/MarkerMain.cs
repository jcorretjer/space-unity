﻿using Assets.Scenes.OneDirection.Combat.C_.Crosshair.Models;
using UnityEngine;

public class MarkerMain : MonoBehaviour
{
    private static Transform markerPos;

    private void Start()
    {
        markerPos = transform;
    }    

    void Update()
    {
        markerPos = transform;

        //Move marker until it reached it's max distance. Abs so that it's always postive
        if (Mathf.Abs(transform.position.z) <= Marker.LIFE_TIME)
            transform.Translate(Marker.InitialMarkerPos * Marker.CONSTANT_SPEED * Time.deltaTime);

        else
            Spawner.DestroyObject(gameObject);
    }
}
