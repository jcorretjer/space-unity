﻿
namespace Assets.Scenes.OneDirection.Combat.C_.Models
{
    public struct ClimaxPoints
    {
        /// <summary>
        /// This indicates which is the speed boosted enemy. For example, .25 means que cada cuarto enemy o es el speed boost o es el spawn climax
        /// </summary>
        public const float QUARTER = 0.25f,
            HALF = .5f,
            THIRD_QUARTER = .75f;
    }
}
