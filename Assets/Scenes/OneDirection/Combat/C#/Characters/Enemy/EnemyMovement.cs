﻿using Assets.Scenes.C_.Models;
using Assets.Scenes.OneDirection.Combat.C_.Characters.Models;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyMovement : MonoBehaviour
{
    private static GameObject playerCam;

    private Character enemy;

    private void Start()
    {
        playerCam = GameObject.Find(string.Format("MainCamera{0}", SceneManager.GetActiveScene().name.Substring(0, 2)));

        enemy = new Character()
        {
            type = Character.Type.ENEMY,

            dmgPts = CamContainerMain.GetSpwn().LvlEnemyDmg,

            movement = new Movement()
            {
                transform = transform,

                ConstantSpeed = CamContainerMain.GetSpwn().LVLSpecific.ConstantSpeed
            }
        };

        if (CamContainerMain.GetSpwn().FindWhenToSpeedBoost(Spawner.GetSpawnCounter()))
        {
            enemy.movement.ConstantSpeed = enemy.movement.ConstantSpeedBoosted;

            Spawner.SetSpawnCounter(0);
        }
    }

    public void SetConstantSpeed(int speed)
    {
        enemy.movement.ConstantSpeed = speed;
    }

    public Character GetEnemy()
    {
        return enemy;
    }
    
    void Update()
    {
        //Stop enemies from moving if player dead or level completed
        if (CamContainerMain.GetPlayer().currentHP > Character.CONSIDERED_DEAD)
        {
            if(Spawner.ShouldEnemiesMove)
                transform.position = Vector3.MoveTowards(transform.position, playerCam.transform.position, enemy.movement.ConstantSpeed * Time.deltaTime);
        }
    }

    public static Vector3 GetPlayerPos()
    {
        return playerCam.transform.position;
    }

}
