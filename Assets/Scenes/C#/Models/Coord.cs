﻿
namespace Assets.Scenes.C_.Models
{
    public struct Coord
    {
        public float x
        {
            get;

            set;
        }

        public float y
        {
            get;

            set;
        }

        public float z
        {
            get;

            set;
        }
    }
}
