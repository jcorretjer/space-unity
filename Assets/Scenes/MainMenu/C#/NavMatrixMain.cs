﻿using Assets.Scenes.C_.Models;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NavMatrixMain : MonoBehaviour
{
    void Start()
    {
        
    }

    public static bool shouldLoadScene = false;

    private void Update()
    {
        if (shouldLoadScene)
        {
            shouldLoadScene = !shouldLoadScene;

            StartCoroutine(LoadSceneAsync(Convert.ToInt32(Game.SceneOrders.CALIBRATION), true));
        }
    }

    private const int SCENE_LOAD_WAIT_TIME = 3000;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sceneIndex"></param>
    /// <param name="shouldWait">Freezes the UI thread for 3 secs before finishing</param>
    /// <returns></returns>
    public static IEnumerator LoadSceneAsync(int sceneIndex, bool shouldWait)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneIndex);

        if (shouldWait)
        {
            System.Threading.Thread.Sleep(SCENE_LOAD_WAIT_TIME);
        }

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    /// <summary>
    /// Loads scene once the hyperspace transition animation is complete
    /// </summary>
    /// <param name="sceneIndex"></param>
    /// <returns></returns>
    public static IEnumerator LoadSceneAsyncAfterHyperSpace(int sceneIndex)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneIndex, LoadSceneMode.Single);

        //Stop the scene from switching until needed too
        asyncLoad.allowSceneActivation = false;

        while (!HyperSpaceMain.AnimationCompleted & !asyncLoad.isDone)
        {
            yield return null;
        }

        //Allow to Switch scene
        asyncLoad.allowSceneActivation = !asyncLoad.allowSceneActivation;
    }
}
