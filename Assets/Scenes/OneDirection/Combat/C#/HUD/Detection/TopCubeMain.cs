﻿using Assets.Scenes.C_.Models;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TopCubeMain : MonoBehaviour
{
    private static TextMeshProUGUI topLbl;    

    public static void Init()
    {
        topLbl = GameObject.Find("DetectionTopLbl").GetComponent<TextMeshProUGUI>();

        topLbl.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals(Tags.ENEMY))
            topLbl.enabled = true;

        else if (other.tag.Equals(Tags.EXPLODING_ENEMY))
        {
            int childCount = other.transform.root.gameObject.GetComponentInChildren<Transform>().childCount;

            //Means that the enemy is on death animation
            if (childCount < 2)
                topLbl.enabled = false;
        }

        //Means that the nothing is colliding except the other cubes
        else
            topLbl.enabled = false;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals(Tags.ENEMY))
            topLbl.enabled = false;
    }
}
