﻿using Assets.Scenes.C_.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackCubeMain : MonoBehaviour
{
    private static GameObject backLbl;

    public static void Init()
    {
        backLbl = GameObject.Find("DetectionBackLbl");

        backLbl.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals(Tags.ENEMY))
            backLbl.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals(Tags.EXPLODING_ENEMY)
            || other.tag.Equals(Tags.ENEMY))
            backLbl.SetActive(false);
    }
}
