﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CameraMain : MonoBehaviour,  IPointerUpHandler, IPointerDownHandler
{
    private static GameObject _gameObj;

    private void Start()
    {
        _gameObj = gameObject;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Spawner.SpawnMarker();

        Spawner.SpawnLaser();

        if(CamContainerMain.GetGameClassObj().IsFXOn)
            GetComponent<AudioSource>().Play();
    }

    public static GameObject GetClickPan()
    {
        return _gameObj;
    }
}
