﻿using UnityEngine;

namespace Assets.Scenes.OneDirection.Combat.C_.Models
{
    public class Crosshair
    {
        public Crosshair()
        {
            Acceleration = new Vector2();

            Rigidbod = new Rigidbody2D();
        }

        public Vector2 Acceleration
        {
            get;

            set;
        }

        public Rigidbody2D Rigidbod
        {
            get;

            set;
        }

        public Transform transform
        {
            get;

            set;
        }
    }
}
