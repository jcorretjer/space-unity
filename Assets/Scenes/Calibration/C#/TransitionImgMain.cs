﻿using Assets.Scenes.C_.Models;
using System;
using UnityEngine;

public class TransitionImgMain : MonoBehaviour
{
    public void StartTransistion()
    {
        GetComponent<Animator>().SetBool("transitionExitShouldStart", true);
    }

    public void LoadOneDirectionCombatScene()
    {
        StartCoroutine(Game.LoadSceneAsync(Convert.ToInt32(CamContainerMain.GetGameClassObj().LevelSelected), false));
    }

    public void LoadCalibrationScene()
    {
        StartCoroutine(Game.LoadSceneAsync(Convert.ToInt32(Game.SceneOrders.CALIBRATION), false));
    }
}
