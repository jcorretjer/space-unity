﻿using Assets.Scenes.C_.Models;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static CamContainerMain;

public class DeathCrackedScreenMain : MonoBehaviour
{
    private static Animator failedAnimator,
        restartAnimator,
        quitAnimator,
        highScoreAnimator;

    private Text restartLbl,
        failLbl,
        quitLbl;

    private static GameObject crackedScreenDeathImg,
         deathExitPan;

    private static Button deathQuitBtn,
        deathRestartBtn;

    void Start()
    {
        //If null beacuse this gets called by every animating thin in the death screen 
        if (failedAnimator == null)
        {
            deathQuitBtn = GameObject.Find("DeathQuitBtn").GetComponent<Button>();

            deathRestartBtn = GameObject.Find("DeathRestartBtn").GetComponent<Button>();

            failedAnimator = GameObject.Find("FailedLbl").GetComponent<Animator>();

            restartAnimator = GameObject.Find("RestartText").GetComponent<Animator>();

            quitAnimator = GameObject.Find("QuitText").GetComponent<Animator>();

            highScoreAnimator = GameObject.Find("HighScorePan").GetComponent<Animator>();

            failLbl = GameObject.Find("FailedLbl").GetComponent<Text>();

            restartLbl = GameObject.Find("RestartText").GetComponent<Text>();

            quitLbl = GameObject.Find("QuitText").GetComponent<Text>();

            deathExitPan = GameObject.Find("DeathExitPan");

            deathExitPan.SetActive(false);

            crackedScreenDeathImg = GameObject.Find("CrackedScreenDeathImg");

            crackedScreenDeathImg.SetActive(false);
        }
    }

    /// <summary>
    /// Display death screen.
    /// Start the mission fail animation.
    /// 
    /// The way this is setup is that once the fail animation ends then starts the restart and once that one is done, starts the quit animation.
    /// </summary>
    public static void StartAnimation()
    {
        //The restart and quit button aren't interatible if this is over them
        CameraMain.GetClickPan().SetActive(false);

        crackedScreenDeathImg.SetActive(true);

        GameObject.Find("CamContainer").GetComponents<AudioSource>()[Convert.ToInt32(CombatAudioSources.LVL_SONG)].Stop();

        if (GetGameClassObj().IsFXOn)
        {          
            GameObject.Find("CamContainer").GetComponents<AudioSource>()[Convert.ToInt32(CombatAudioSources.SCREEN_HIT)].Play();
        }

        failedAnimator.SetBool("hasFailed", true);
    }

    /// <summary>
    /// Used from failedLbl
    /// </summary>
    public void StartRestartAnimation()
    {
        restartAnimator.SetBool("startRestartText", true);
    }

    /// <summary>
    /// Used from restart btn txt lbl
    /// </summary>
    public void StartQuitAnimation()
    {
        deathRestartBtn.interactable = !deathRestartBtn.interactable;

        quitAnimator.SetBool("startQuitText", true);
    }

    /// <summary>
    /// The btns on the death screen are disabed until they become visible.
    /// 
    /// Used on the death screen quit btn
    /// </summary>
    public void EnableDeathQuitBtn()
    {
        deathQuitBtn.interactable = deathRestartBtn.interactable;

        Game mainGame = GetGameClassObj();

        if (GetGameClassObj().Mode == Game.Modes.ENDLESS)
        {
            if(mainGame.CurrentScore > mainGame.HighScore)
            {
                mainGame.HighScore = mainGame.CurrentScore;

                SetGameClassObj(mainGame);

                TextMeshProUGUI txt = GameObject.Find("HighScoreTitleLbl").GetComponent<TextMeshProUGUI>();

                txt.text = string.Format("New {0}!", txt.text);
            }

            GameObject.Find("HighScoreLbl").GetComponent<TextMeshProUGUI>().SetText(mainGame.HighScore.ToString());

            highScoreAnimator.SetBool("StartScore", true);
        }
    }

    public void ToggleDeathExitPan(bool active)
    {
        if (GetGameClassObj().IsFXOn)
            GameObject.Find("CamContainer").GetComponents<AudioSource>()[Convert.ToInt32(CombatAudioSources.BTN_CLICK)].Play();

        deathExitPan.SetActive(active);
    }
}
