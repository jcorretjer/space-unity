﻿using Assets.Scenes.C_.Models;
using System;
using UnityEngine;

public class ConfirmationMain : MonoBehaviour
{
    private static GameObject pauseCanvas;

    public enum AudioSrcs
    {
        CALIBRE = 0,

        BTN_CLICK
    }

    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        #region Make confirmation invisible

        pauseCanvas = GameObject.Find("ConfirmationCanvas");

        pauseCanvas.SetActive(!pauseCanvas.activeSelf); 

        #endregion
    }

    /// <summary>
    /// Toggle visibility of the confirmation canvas
    /// </summary>
    public static void ToggleConfirmation()
    {
        pauseCanvas.SetActive(!pauseCanvas.activeSelf);

        if(pauseCanvas.activeSelf)
            Screen.sleepTimeout = Game.DEFAULT_SCREEN_TIMEOUT;

        else
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    public void Confirm()
    {
        if(CamContainerMain.GetGameClassObj().IsFXOn)
            GameObject.Find("CamContainer").GetComponents<AudioSource>()[Convert.ToInt32(AudioSrcs.BTN_CLICK)].Play();

        ToggleConfirmation();
    }

    public void Decline()
    {
        if (CamContainerMain.GetGameClassObj().IsFXOn)
            GameObject.Find("CamContainer").GetComponents<AudioSource>()[Convert.ToInt32(AudioSrcs.BTN_CLICK)].Play();

        pauseCanvas.SetActive(!pauseCanvas.activeSelf);
    }

}
