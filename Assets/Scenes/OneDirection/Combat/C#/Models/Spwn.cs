﻿using System;
using System.Collections.Generic;

namespace Assets.Scenes.OneDirection.Combat.C_.Models
{
    /// <summary>
    /// Enemy spawner model
    /// </summary>
    public class Spwn
    {
        private Random random,
            randomClimaxPoint;

        public const int MAX_ENEMIES_ALIVE_LIMIT = 6,
            //DEFAULT_LVL_ENEMY_DMG = 1;
        DEFAULT_LVL_ENEMY_DMG = 6;

        public Spwn(float climaxPoint, float speedBoostPoint, string lvlName)
        {
            LVLSpecific = new LVLSpecific().GetLVLSpecificByLvlName(lvlName);

            MaxTotal = LVLSpecific.MaxTotalSpawn;

            MinTotal = MaxTotal - 5;

            random = new Random();

            ClimaxPoint = climaxPoint;

            SpeedBoostPoint = speedBoostPoint;

            LvlEnemyDmg = DEFAULT_LVL_ENEMY_DMG;
        }

        /// <summary>
        /// Randomly select the climax and speed boost point
        /// </summary>
        public Spwn(string lvlName)
        {
            LVLSpecific = new LVLSpecific().GetLVLSpecificByLvlName(lvlName);

            MaxTotal = LVLSpecific.MaxTotalSpawn;

            MinTotal = MaxTotal - 5;

            LvlEnemyDmg = DEFAULT_LVL_ENEMY_DMG;

            random = new Random();

            List<float> climaxPoints = new List<float>();

            climaxPoints.Add(ClimaxPoints.HALF);

            climaxPoints.Add(ClimaxPoints.QUARTER);

            climaxPoints.Add(ClimaxPoints.THIRD_QUARTER);

            randomClimaxPoint = new Random();

            ClimaxPoint = climaxPoints[randomClimaxPoint.Next(climaxPoints.Count)];

            SpeedBoostPoint = climaxPoints[randomClimaxPoint.Next(climaxPoints.Count)];
        }

        public LVLSpecific LVLSpecific
        {
            get;

            set;
        }

        public float SpeedBoostPoint
        {
            get;

            set;
        }

        public float ClimaxPoint
        {
            get;

            set;
        }

        public float EndPoint
        {
            get;

            set;
        }

        public int MaxTotal
        {
            get;

            set;
        }

        public int MinTotal
        {
            get;

            set;
        }

        public int TotalAmount
        {
            get;

            set;
        }

        public float LvlEnemyDmg
        {
            get;

            set;
        }

        /// <summary>
        /// This value lets you know which enemy will be the one that will have the speed boost 
        /// </summary>
        public int SpeedBoostIndicator
        {
            get;

            set;
        }

        /// <summary>
        /// This value lets you know the count of enemies till the spwn timer reaches it's climax or fastest point
        /// </summary>
        public float SpawnClimaxIndicator
        {
            get;

            set;
        }

        /// <summary>
        /// Randomly generates the total amount to spawn using Min total and MaxToal as seeds
        /// </summary>
        /// <returns>Total amount randomly generated</returns>
        public int CalcTotalAmount()
        {
            TotalAmount = random.Next(MinTotal, MaxTotal);

            return TotalAmount;
        }

        /// <summary>
        /// Calculates the distributed sum total damage needed to kill the player on this level. 
        /// In other words, if all enemies were to hit how much damage do they all need to kill the player.
        /// </summary>
        /// <param name="maxHP">Player max hp.</param>
        /// <returns></returns>
        //public float CalcLvlEnemyDmg(float maxHP)
        //{
        //    LvlEnemyDmg = maxHP / TotalAmount;

        //    return LvlEnemyDmg;
        //}

        /// <summary>
        /// Identifies when an enemy should be speed boosted.
        /// </summary>
        /// <param name="spawnCount"></param>
        /// <returns></returns>
        public bool FindWhenToSpeedBoost(int spawnCount)
        {
            SpeedBoostIndicator = Convert.ToInt32(TotalAmount * SpeedBoostPoint);

            return spawnCount == Convert.ToInt32(TotalAmount * SpeedBoostPoint);
        }

        /// <summary>
        /// Calculates the enemy count value needed to reach the climax spawn time
        /// </summary>
        public void CalcSpawnClimaxNEndIndicator()
        {
            SpawnClimaxIndicator = TotalAmount * ClimaxPoint;
        }

        //If we ever wanted to modify how quickly the spawn on the starting, climax or end point, we would have to play around with these values. 
        public const int STARTING_SPAWN_TIME = 3000,
            ENDING_SPAWN_TIME = 1700,
        CLIMAX_SPAWN_TIME = 1000,
            ENDLESS_SPAWN_TIME_SUBSTRACTION = 300;

        /// <summary>
        /// Calculates how much is needed to add/substract to reach the spawn climax based on how many enemies is need to reach spawn climax
        /// </summary>
        /// <returns></returns>
        public float CalcSpawnTimeSubstractor()
        {
            return ( STARTING_SPAWN_TIME - CLIMAX_SPAWN_TIME ) / SpawnClimaxIndicator;            
        }
    }
}
