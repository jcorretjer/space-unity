﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scenes.C_.Models
{
     public class Game
    {
        public enum SceneOrders
        {
            SPLASH_SCREEN = 0,

            MAIN_MENU,

            CALIBRATION,

            ONE_DIRECTION,

            CHINITA,

            AMARILLO,

            VERDE,

            AZUL,

            INDIGO,

            VIOLETA,

            CREDITS
        }

        public enum Modes
        {
            STORY,

            ENDLESS
        }

        public Game()
        {
            CurrentGyroInfo = new GyroInfo();

            IsMusicOn = true;

            IsFXOn = IsMusicOn;

            CurrentScreenRes = string.Empty;

            Mode = Modes.STORY;

            LevelSelected = SceneOrders.ONE_DIRECTION;

            Testing = false;

            Unlocks = new Unlocks();

            HighScore = 0;

            CurrentScore = HighScore;
        }

        public bool SupportsGyro
        {
            get;

            set;
        }

        public SceneOrders LevelSelected
        {
            get;

            set;
        }

        public GyroInfo CurrentGyroInfo
        {
            get;

            set;
        }

        /// <summary>
        /// Got to trigger enther of orientationconfig to find definition
        /// </summary>
        public GyroInfo TempGyroInfo
        {
            get;

            set;
        }

        public Modes Mode
        {
            get;

            set;
        }

        public bool IsMusicOn
        {
            get;

            set;
        }

        public bool IsFXOn
        {
            get;

            set;
        }

        private const int SCENE_LOAD_WAIT_TIME = 3000;

        public static IEnumerator LoadSceneAsync(int sceneIndex, bool shouldWait)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneIndex);

            if (shouldWait)
            {
                System.Threading.Thread.Sleep(SCENE_LOAD_WAIT_TIME);
            }

            while (!asyncLoad.isDone)
            {
                yield return null;
            }
        }

        public const float DEFAULT_SKYBOX_EXPOSURE = 1.05f;

        /// <summary>
        /// Runs a lerp that changes exposure of the skybox
        /// </summary>
        /// <param name="skybox">Current skybox material</param>
        /// <param name="to">The final value of the exposure</param>
        /// <param name="duration">For how long will the lerp last</param>
        /// <returns></returns>
        public static IEnumerator SetSkyboxExposure(Material skybox, float to, float duration)
        {
            float exposure =  skybox.GetFloat("_Exposure");

            while (exposure > (to + 0.02))
            {
                exposure = Mathf.Lerp(exposure, to, duration);

                skybox.SetFloat("_Exposure", exposure);

                yield return null;
            }
        }

        /// <summary>
        /// Lowers the volume until it reaches 0 in the time span of 30 seconds
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public static IEnumerator FadeOutMusic(AudioSource src)
        {
            while (src.volume > 0)
            {
                src.volume = Mathf.Lerp(src.volume, 0, Time.deltaTime * 30);

                yield return null;
            }
        }

        /// <summary>
        /// Canvas UI scalemode to screen size can only support 1920 and below, anything above that just doesn't work so well. 
        /// To increase that support it was necesary to create aditional copies of all the UI elements that were out of places when using higher resolutions,
        /// scale them and position them relative to the current resolution. The name of these scaled versions end with the resolution number and that's what this variable is for.
        /// The name format is the following 'name0000' 0000 being the resolution numer.
        /// Anytime we need to access one of those scaled UIs we call it's name + this variable.
        /// 
        /// </summary>
        public string CurrentScreenRes
        {
            get;

            set;
        }

        /// <summary>
        /// Indicates if the game is in a testing mode
        /// </summary>
        public bool Testing
        {
            get;

            set;
        }

        public const int DEFAULT_SCREEN_TIMEOUT = 20;

        public static int GetSceneIndexBySceneName(string sceneName)
        {
            if (sceneName.Contains(SceneNames.AMARILLO))            
                return Convert.ToInt32(SceneOrders.AMARILLO);

            else if (sceneName.Contains(SceneNames.AZUL))
                return Convert.ToInt32(SceneOrders.AZUL);

            else if (sceneName.Contains(SceneNames.CHINITA))
                return Convert.ToInt32(SceneOrders.CHINITA);

            else if (sceneName.Contains(SceneNames.INDIGO))
                return Convert.ToInt32(SceneOrders.INDIGO);

            else if (sceneName.Contains(SceneNames.RED))
                return Convert.ToInt32(SceneOrders.ONE_DIRECTION);

            else if (sceneName.Contains(SceneNames.VERDE))
                return Convert.ToInt32(SceneOrders.VERDE);

            else
                return Convert.ToInt32(SceneOrders.VIOLETA);
        }

        public Unlocks Unlocks
        {
            get;

            set;
        }

        public int HighScore
        {
            get;

            set;
        }

        public int CurrentScore
        {
            get;

            set;
        }

        /// <summary>
        /// Get the high score based on what's higher
        /// </summary>
        /// <returns>The current high score if the current score is lower or the current score if it's higher than the high score</returns>
        public int CalcHighScore()
        {
            return CurrentScore > HighScore
                ? CurrentScore
                : HighScore;
        }
    }
}
