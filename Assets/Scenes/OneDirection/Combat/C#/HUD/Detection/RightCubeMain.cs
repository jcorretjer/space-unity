﻿using Assets.Scenes.C_.Models;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RightCubeMain : MonoBehaviour
{
    private static TextMeshProUGUI rightLbl;

    public static void Init()
    {
        rightLbl = GameObject.Find("DetectionRightLbl").GetComponent<TextMeshProUGUI>();

        rightLbl.enabled = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals(Tags.ENEMY))
        {
            rightLbl.enabled = true;
        }

        else if (other.tag.Equals(Tags.EXPLODING_ENEMY))
        {
            int childCount = other.transform.root.gameObject.GetComponentInChildren<Transform>().childCount;

            //Means that the enemy is on death animation
            if (childCount < 2)
                rightLbl.enabled = false;
        }

        //Means that the nothing is colliding except the other cubes
        else
            rightLbl.enabled = false;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals(Tags.ENEMY))
            rightLbl.enabled = false;
    }
}
