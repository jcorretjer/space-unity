﻿using UnityEngine;

namespace Assets.Scenes.OneDirection.Combat.C_.Characters.Models
{
    public class Movement
    {
        public const float SPEED_BOOST_INCREASE = 2;

        private float constantSpeed;

        public float ConstantSpeed
        {
            get
            {
                return constantSpeed;
            }

            set
            {
                constantSpeed = value;

                ConstantSpeedBoosted = value + SPEED_BOOST_INCREASE;
            }
        }

        /// <summary>
        /// This is the constant speed plus the speed booster
        /// </summary>
        public float ConstantSpeedBoosted
        {
            get;

            set;
        }

        public Transform transform
        {
            get;

            set;
        }
    }
}
