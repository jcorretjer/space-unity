﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scenes.C_.Models
{
    [Serializable]
    public class Unlocks
    {
        public Unlocks()
        {
            Rojo = true;

            Amarillo = false;

            Azul = false;

            Chinita = false;

            Indigo = false;

            Verde = false;

            Violeta = false;
        }

        public bool Amarillo
        {
            get;

            set;
        }

        public bool Azul
        {
            get;

            set;
        }

        public bool Chinita
        {
            get;

            set;
        }

        public bool Indigo
        {
            get;

            set;
        }

        public bool Rojo
        {
            get;

            set;
        }

        public bool Verde
        {
            get;

            set;
        }

        public bool Violeta
        {
            get;

            set;
        }
    }
}
