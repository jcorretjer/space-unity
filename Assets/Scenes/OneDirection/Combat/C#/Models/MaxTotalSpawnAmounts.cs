﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scenes.OneDirection.Combat.C_.Models
{
    public struct MaxTotalSpawnAmounts
    {
        public const int LVL_7 = 106,
            LVL_6 = 92,
            LVL_5 = 76,
            LVL_4 = 62,
            LVL_3 = 48,
            LVL_2 = 33,
            LVL_1 = 20;
    }
}
