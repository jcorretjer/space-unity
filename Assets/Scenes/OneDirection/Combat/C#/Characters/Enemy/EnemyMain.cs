﻿using Assets.Scenes.OneDirection.Combat.C_.Characters.Models;
using Assets.Scenes.OneDirection.Combat.C_.Models;
using UnityEngine;

public class EnemyMain : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //If hit by laser
        if (other.tag.Contains("Laser"))
        {
            CamContainerMain.AddOneHitCount();

            CamContainerMain.GetGameClassObj().CurrentScore += Spwn.DEFAULT_LVL_ENEMY_DMG;

            Spawner.DestroyObject(other.gameObject);

            DeleteEnemy(gameObject, true);

            VerifyIfLVLCompleted();
        }

        //If hit by player
        else if (other.tag.Contains("Cam"))
        {
            Character player = CamContainerMain.GetPlayer();

            //This is the real one. The line below is used to bypass the test flag
            if (!CamContainerMain.GetGameClassObj().Testing)
            //if (CamContainerMain.GetGameClassObj().Testing)
            {
                #region Player hp stuff

                Vector3 centerHpBarScale = CamContainerMain.GetCenterHpBar().transform.localScale;

                //Find the current enemy container game object, pull the script and get how much dmg it does
                centerHpBarScale = player.CalculateHPBar(
                    gameObject.transform.parent.gameObject
                    .GetComponent<EnemyMovement>()
                    .GetEnemy().dmgPts,
                    centerHpBarScale.y
                    );

                CamContainerMain.SetCenterHpBarScale(centerHpBarScale);

                #endregion
            }

            //Play only if greater than 0 cause once it reaches 0 game over animation kicks in and this one is ignored
            if (player.currentHP > Character.CONSIDERED_DEAD)
            {
                CrackedScreenMain.StartHitAnimation();

                CamContainerMain.AddOneHitCount();

                VerifyIfLVLCompleted();
            }

            //Play death animation instead
            else
                DeathCrackedScreenMain.StartAnimation();

            DeleteEnemy(gameObject, false);
        }

        //If hit by marker, set the laser to target the enemy coordinates and delete this marker. 
        else if (other.tag.Contains("Marker"))
        {
            Spawner.AddMarkedEnemy(other.gameObject.name, transform);

            Spawner.DestroyObject(other.gameObject);
        }
    }

    private void VerifyIfLVLCompleted()
    {
        if (CamContainerMain.GetHitCount() == CamContainerMain.GetSpwn().TotalAmount)
        {
            Spawner.StopSpawningEnemies();

            GameObject.Find("Orange").GetComponent<Animator>().SetTrigger("ChangeLvl");
        } 
    }

    /// <summary>
    /// Set the scale value of the current hp bar based on the current. 
    /// </summary>
    /// <param name="player"></param>
    public static void ReScaleHPBar(Character player)
    {
        Vector3 centerHpBarScale = CamContainerMain.GetCenterHpBar().transform.localScale;

        //Find the current enemy container game object, pull the script and get how much dmg it does
        centerHpBarScale = player.CalculateHPBar(centerHpBarScale.y);

        CamContainerMain.SetCenterHpBarScale(centerHpBarScale);
    }

    /// <summary>
    /// Delete the enemy game object and play the death animation
    /// </summary>
    /// <param name="enemyObj">Object to be deleted</param>
    /// <param name="addForceToAnimation">True = the enemy parts will fly all over the place</param>
    private void DeleteEnemy(GameObject enemyObj, bool addForceToAnimation)
    {
        Destroy(enemyObj);

        if(addForceToAnimation && CamContainerMain.GetGameClassObj().IsFXOn)
            enemyObj.transform.parent.gameObject.GetComponent<AudioSource>().Play();

        //Busca el parent y usa el child con index 1 que ese es el shard
        Explosion.SetJustDied(enemyObj.transform.parent.gameObject.transform.GetChild(1).gameObject, addForceToAnimation);
    }

    public static string GetParentContainerName(string childObjName)
    {
        string parentName = "EnemyContainer";

        //All enemy meshes have number ID after their names which goes "name-ID".
        string[] nameSplit = childObjName.Split('-');

        //The container has the same ID as the child mesh but formatted like "name ID" 
        if (nameSplit.Length > 1)
            parentName += string.Format(" {0}", nameSplit[1]);

        return parentName;
    }
}
