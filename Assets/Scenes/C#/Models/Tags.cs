﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scenes.C_.Models
{
    public struct Tags
    {
        public const string ENEMY_CONTAINER = "EnemyContainer",
            EXPLODING_ENEMY = "ExplodingEnemy",
            ENEMY = "Enemy",
            UNLOCKS = "Unlocks",
            HIGH_SCORE = "HighScore";
    }
}
