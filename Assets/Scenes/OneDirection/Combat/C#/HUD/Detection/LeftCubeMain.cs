﻿using Assets.Scenes.C_.Models;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LeftCubeMain : MonoBehaviour
{
    private static TextMeshProUGUI leftLbl;

    public static void Init()
    {
        leftLbl = GameObject.Find("DetectionLeftLbl").GetComponent<TextMeshProUGUI>();

        leftLbl.enabled = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals(Tags.ENEMY))
        {
            leftLbl.enabled = true;
        }

        else if (other.tag.Equals(Tags.EXPLODING_ENEMY))
        {
            int childCount = other.transform.root.gameObject.GetComponentInChildren<Transform>().childCount;

            //Means that the enemy is on death animation
            if (childCount < 2)
                leftLbl.enabled = false;
        }

        //Means that the nothing is colliding except the other cubes
        else
            leftLbl.enabled = false;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals(Tags.ENEMY))
            leftLbl.enabled = false;
    }
}
