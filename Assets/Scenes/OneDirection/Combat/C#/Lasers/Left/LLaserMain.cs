﻿using Assets.Scenes.OneDirection.Combat.C_.Lasers.Models;
using UnityEngine;

public class LLaserMain : MonoBehaviour
{
    private static GameObject game;

    public static Laser laser;

    void Start()
    {
        laser = new Laser();

        game = gameObject;
    }

    void Update()
    {
        Vector3 laserPos = transform.position;

        //Every time a laser is created it's object name is formatted like this:'name-id-clone'. Split by - that way I can use the ID
        string[] nameSlipt = gameObject.name.Split('-');

        Transform markerPos = transform;

        /*
         * Lasers are created everytime it's clicked. Each one has a count/ID. Once an enemy is marked, get its position.
         * 
         * Each marker has a laser. So if the marker ID is 1 then it's brother laser is 1.
         */
        Transform enemyTransform = Spawner.GetEnemyMarked(string.Format("CrossMarker-{0}-(Clone)", nameSlipt[1]));

        //Check for null because enemy objs get destroyed and stop existing.
        if (enemyTransform != null)
        {
            markerPos = enemyTransform;
        }

        //If it hasn't found an enemy. Once the marker reaches it's max distance, this laser is destroyed as well.
        else
        {
            GameObject marker = GameObject.Find(string.Format("CrossMarker-{0}-(Clone)", nameSlipt[1]));

            if (marker != null)
            {
                markerPos = marker.transform;
            }

            else
                Spawner.DestroyObject(gameObject);
        }

        if (laser.ShouldMove)
        {
            transform.position = Vector3.MoveTowards(transform.position, markerPos.position, Laser.ConstantSpeed * Time.deltaTime);
        }
    }
}
